
#ifndef DARK_FIRSTDZ_H
#define DARK_FIRSTDZ_H


int findDegree(int i);
int findLog(int x);
int power(int x, unsigned p);

#endif //DARK_FIRSTDZ_H
