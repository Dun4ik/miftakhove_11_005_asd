#include "math.h"
#include "iostream"
#include "firstDz.h"
using namespace std;
int main(){
    //Task 1

    int n;
    cin >> n;
    int arr[n];
    int sum = 0;

    for(int i = 0; i < n; i++){
        cin >> arr[i];
        sum += arr[i];
    }
    cout << sum << endl;

    //Task 2

    double a,b,c;
    double x;
    cin >> a >> b >> c;

    double desc = b * b - 4 * a * c;
    if(desc > 0) {
        x = (sqrt(desc) - b)/(2 * a);
        cout << x << " ";
        x = (- sqrt(desc) - b)/(2 * a);
        cout << x << endl;
    }
    else if(desc == 0){
        x = sqrt(desc)/(2 * a);
        cout << x << endl;
    }
    else
        cout << "NO" << endl;

    //Task 3

    int x3;
    cin >> x3;

    cout << findLog(x3) << endl;

    //Task 4
    int x4;
    unsigned p;
    cin >> x4 >> p;
    cout << power(x4, p) << endl;

    return 0;
}
int power(int x, unsigned p){
    int result = 1;
    for (int i = 0; i < p; ++i)
        result *= x;
    return result;
}
int findLog(int x) {
    if(x < 1) {
        cout << "Invalid, enter x >= 1" << endl;
        cin >> x;
        return findLog(x);
    }
    else{
        int i = 0;
        int degree = findDegree(i);
        while(degree <= x) {
            i++;
            degree = findDegree(i);
        }
        if(degree > x)
            return --i;
        return i;
    }
}
int findDegree(int i) {
    int j = 0;
    int result = 1;
    do{
       if(i == 0)
           return result;
       else
           result *= 2;
       j++;
    }while(j <= i);
    return result;
}