#include "fourth.h"
#include "iostream"
using namespace std;
struct Node{
    int item;
    Node * next;
    Node * last;
};
struct DoubleLinkedList{
    Node * head = nullptr;
    int lSize = 0;

    void add(int item){
        Node * node = new Node;
        node->item = item;
        node->next = nullptr;
        node->last = nullptr;

        if(head == nullptr)
            head = node;
        else{
            head->next = node;
            Node * current = head;
            head = head->next;
            head->last = current;
        }
        lSize++;
    }
    int size(){
        return lSize;
    }
    int get(int id) {
        Node *current = head;
        for (int i = lSize - 1; i >= 0; --i) {
            if (id == i)
                return current->item;
            current = current->last;
        }
    }
    void insertAt(int id, int item){
        Node * node = new Node;
        Node * current = head;
        node->item = item;

        for (int i = lSize - 1; i >= 0; --i) {
            if(lSize <= id) {
                cout << "This id doesn't exist" << endl;
                break;
            }
            if(id == i){
                Node * last = current->last;

                if(id != 0)
                    current->last->next = node;
                node->last = last;
                current->last = node;
                node->next = current;

                lSize++;
                break;
            }
            current = current->last;
        }
    }
    void removeAt(int id){
        Node * current = head;
        for (int i = lSize - 1; i >= 0; --i) {
            if(lSize <= id) {
                cout << "This id doesn't exist" << endl;
                break;
            }
            if(id == i){
                Node * last = current->last;
                Node * next = current->next;

                last->next = next;
                next->last = last;
                delete current;
                if(id == lSize - 1)
                    head = last;
                lSize--;
                break;
            }
            else
                current = current->last;
        }
    }
    void printAll(){
        for (int i = 0; i < lSize; ++i) {
            cout << get(i) << " ";
        }
        cout << endl;
    }
};
struct Queue{
    Node * pHead = nullptr;
    Node * pEnd = nullptr;
    int qSize = 0;

    void Enqueue(int item){
        Node * node = new Node;
        node->item = item;

        if(pHead == nullptr)
            pHead = node;
        else {
            pHead->last = node;
            node->next = pHead;
            if(pEnd == nullptr) {
                pEnd = pHead;
                pEnd->last = node;
            }
            pHead = node;
        }
        qSize++;
    }
    int Dequeue(){
        if(qSize < 1)
            return -1;
        Node * current = pEnd->last;
        int item = pEnd->item;
        delete pEnd;
        pEnd = current;
        qSize--;
        return item;
    }
};
int main(){
    DoubleLinkedList * list = new DoubleLinkedList;
    list->add(13);
    list->add(45);
    list->add(62);
    list->add(8877);

    cout << list->get(0) << endl;
    cout << list->get(1) << endl;
    cout << list->get(2) << endl;
    cout << list->get(3) << endl;

    list->printAll();

    list->insertAt(0, 9);
    list->insertAt(2, 0);
    list->insertAt(5,0);
    list->printAll();

    list->removeAt(4);
    list->printAll();

    Queue * q = new Queue;
    q->Enqueue(12);
    q->Enqueue(54);
    q->Enqueue(775);

    cout << q->Dequeue() << endl;
    cout << q->Dequeue() << endl;
    cout << q->Dequeue() << endl;
    cout << q->Dequeue() << endl;
}