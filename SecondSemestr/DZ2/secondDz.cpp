#include "secondDz.h"
#include "iostream"
using namespace std;
int main(){
    //Task 1
    {
        int a[5];
        for (int i = 0; i < 5; ++i)
            cin >> a[i];
        cout << sum(a, 5) << endl;
    }
    //Task 2
    {
        int * a1 = new int[5];
        int * a2 = new int[5];

        for (int i = 0; i < 5; ++i)
            cin >> *(a1 + i);
        for (int i = 0; i < 5; ++i)
            cin >> *(a2 + i);

        int * a3 = new int[10];

        for (int i = 0; i < 5; ++i)
            *(a3 + i) = *(a1 + i);
        for (int i = 0; i < 5; ++i)
            *(a3 + i + 5) = *(a2 + i);

        for (int i = 0; i < 10; ++i)
            cout << *(a3 + i) << " ";
        delete[] a1;
        delete[] a2;
        cout << endl;
    }
    //Task 3
    {
        int * a = new int[5];

        for (int i = 0; i < 5; ++i)
            cin >> *(a + i);

        sort(a, 5);

        for (int i = 0; i < 5; ++i)
            cout << *(a + i) << " ";
        cout << endl;
        delete[] a;
    }
    return 0;
}
int sum(int * a, int size){
    int all = 0;
    for (int i = 0; i < size; ++i)
        all += *(a + i);
    return all;
}
void sort(int * a, int size){
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            if(*(a + j) > *(a + j + 1) && (j + 1) < size){
                int t = *(a + j);
                *(a + j) = *(a + j + 1);
                *(a + j + 1) = t;
            }
        }
    }
}