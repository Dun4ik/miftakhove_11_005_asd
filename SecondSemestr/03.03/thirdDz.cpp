#include "thirdDz.h"
#include "iostream"
using namespace std;

struct Node{
    int item;
    Node * next;
};
struct Stack{
    Node * pHead;
    Node * returnHead;
    int size = 0;

    void Push(int item){
        Node * node = new Node;
        node->item = item;
        node->next = pHead;
        pHead = node;
        returnHead = pHead;
        size++;
    }
    int Pop(){
        Node * temp = pHead;
        int x = temp->item;
        pHead = temp->next;
        returnHead = pHead;
        delete temp;
        size--;
        return x;
    }
    int Peek(){
        if(!pHead)
            throw 1;
        return pHead->item;
    }
    void PrintAll(){
        int sizePrint = size;
        while(sizePrint > 0){
            int x = pHead->item;
            cout << x << " ";
            pHead = pHead->next;
            sizePrint--;
        }
        pHead = returnHead;
    }
    int GetSize(){
        return size;
    }
};
struct LinkedList{
    int item;
    Node * pHead = nullptr;
    int size = 0;

    void Add(int item) {
        Node *node = new Node;

        node->item = item;
        node->next = nullptr;

        if (pHead == nullptr) {
            pHead = node;
        }
        else {
            Node * current = pHead;
            for (int i = 0; i < size - 1; ++i) {
                current = current->next;
            }
            current->next = node;
        }
        size++;
    }
    int Size(){
        return size;
    }
    int Get(int id){
        Node * current = pHead;
        for (int i = 0; i < id; ++i)
            current = current->next;
        int x = current->item;
        return x;
    }
    void Remove(int id){
        Node * current = pHead;
        for (int i = 0; i < id - 1; ++i)
            current = current->next;
        if(id != 0 && id != size - 1){
            Node * tempLast = current;
            current = current->next;

            Node * temp = current;
            current = current->next;

            tempLast->next = current;
            delete temp;
        }
        else {
            current = current->next;
            delete current;
        }
        size--;
    }
    void PrintAll(){
        Node * current = pHead;
        for (int i = 0; i < size; ++i) {
            cout << current->item << " ";
            current = current->next;
        }
    }
};

int main(){
    //Task 1
    Stack * stack = new Stack;

    stack->Push(12);
    stack->Push(24);
    stack->Push(36);
    stack->Push(48);
    stack->Push(50);

    cout << stack->GetSize() << endl;
    stack->PrintAll();
    cout << endl;

    stack->Pop();
    stack->Pop();

    cout << stack->GetSize() << endl;
    stack->PrintAll();
    cout << endl;

    delete stack;

    LinkedList * list = new LinkedList;

    list->Add(1);
    list->Add(2);
    list->Add(4);
    list->Add(3);

    cout << list->Size() << endl;
    cout << list->Get(3) << endl;

    list->PrintAll();
    list->Remove(3);
    cout << endl;
    list->PrintAll();

    delete list;
    return 0;
}