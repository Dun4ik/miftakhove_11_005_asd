//
// Created by My PC on 08.04.2021.
//

#ifndef DARK_FIFTHDZ_H
#define DARK_FIFTHDZ_H

struct Node;
struct SortedList;
struct SortedUnicList;
void fillList(SortedList * a, SortedList * b);
SortedList * Union(SortedList * a, SortedList * b);
SortedList * Intersect(SortedList * a, SortedList * b);
SortedList * Difference(SortedList * a, SortedList * b);


#endif //DARK_FIFTHDZ_H
