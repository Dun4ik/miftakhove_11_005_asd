#include "fifthDz.h"
#include "iostream"
using namespace std;
struct Node{
    int item;
    Node * next;
    Node * last;
};
struct SortedList{
    Node * pHead = nullptr;
    int lSize = 0;

    void add(int item){
        Node * node = new Node;
        node->item = item;
        node->next = nullptr;
        node->last = nullptr;

        if(pHead == nullptr)
            pHead = node;
        else{
            Node * current = pHead;
            for (int i = 0; i < lSize; ++i) {
                if(current->item > item){
                    if(current->last != nullptr) {
                        Node *last = current->last;
                        last->next = node;
                        node->last = last;
                    }
                    current->last = node;
                    node->next = current;
                    if(current == pHead)
                        pHead = node;
                }
                else if(current->next == nullptr){
                    current->next = node;
                    node->last = current;
                }
                else
                    current = current->next;
            }
        }
        lSize++;
    }
    int get(int id){
        Node * current = pHead;
        for (int i = 0; i < lSize; ++i) {
            if(id == i)
                return current->item;
            else
                current = current->next;
        }
    }
    int size(){
        return lSize;
    }
    void remove(int id){
        Node * current = pHead;
        for (int i = 0; i < lSize; ++i) {
            if(id == i){
                Node * last = current->last;
                Node * next = current->next;

                if(last != nullptr)
                    last->next = next;
                if(next != nullptr)
                    next->last = last;

                if(i == 0)
                    pHead = next;
                delete current;
            }
            else
                current = current->next;
        }
        lSize--;
    }
    void printAll(){
        Node * current = pHead;
        for (int i = 0; i < lSize; ++i) {
            cout << current->item << " ";
            current = current->next;
        }
        cout << endl;
    }
};
struct SortedUnicList{
    Node * pHead = nullptr;
    int lSize = 0;

    void add(int item){
        Node * node = new Node;
        node->item = item;
        node->next = nullptr;
        node->last = nullptr;

        if(pHead == nullptr)
            pHead = node;
        else{
            Node * current = pHead;
            for (int i = 0; i < lSize; ++i) {
                if (current->item != item) {
                    if (current->item > item) {
                        if (current->last != nullptr) {
                            Node *last = current->last;
                            last->next = node;
                            node->last = last;
                        }
                        current->last = node;
                        node->next = current;
                        if (current == pHead)
                            pHead = node;
                    } else if (current->next == nullptr) {
                        current->next = node;
                        node->last = current;
                    } else
                        current = current->next;
                }
                else
                    return;
            }
        }
        lSize++;
    }
    int get(int id){
        Node * current = pHead;
        for (int i = 0; i < lSize; ++i) {
            if(id == i)
                return current->item;
            else
                current = current->next;
        }
    }
    int size(){
        return lSize;
    }
    void remove(int id){
        Node * current = pHead;
        for (int i = 0; i < lSize; ++i) {
            if(id == i){
                Node * last = current->last;
                Node * next = current->next;

                if(last != nullptr)
                    last->next = next;
                if(next != nullptr)
                    next->last = last;

                if(i == 0)
                    pHead = next;
                delete current;
            }
            else
                current = current->next;
        }
        lSize--;
    }
    void printAll(){
        Node * current = pHead;
        for (int i = 0; i < lSize; ++i) {
            cout << current->item << " ";
            current = current->next;
        }
        cout << endl;
    }
};
SortedList * Union(SortedList * a, SortedList * b){
    SortedUnicList * list = new SortedUnicList;
    SortedList * sortedList = new SortedList;
    for (int i = 0; i < a->size(); ++i)
        list->add(a->get(i));
    for (int i = 0; i < b->size(); ++i)
        list->add(b->get(i));
    for (int i = 0; i < list->size(); ++i)
        sortedList->add(list->get(i));
    return sortedList;
}
SortedList * Intersect(SortedList * a, SortedList * b){
    SortedList * list = new SortedList;
    for (int i = 0; i < a->size(); ++i) {
        int x = a->get(i);
        for (int j = 0; j < b->size(); ++j) {
            if(x == b->get(j)){
                list->add(x);
                b->remove(j);
                j--;
            }
        }
    }
    return list;
}
SortedList * Difference(SortedList * a, SortedList * b){
    for (int i = 0; i < a->size(); ++i) {
        int x = a->get(i);
        for (int j = 0; j < b->size(); ++j) {
            if(x == b->get(j)){
                a->remove(i);
                i--;
                break;
            }
        }
    }
    return a;
}
void fillList(SortedList * a, SortedList * b){
    for (int i = 0; i < 10; ++i) {
        int x;
        cin >> x;
        if (i < 5)
            a->add(x);
        else
            b->add(x);
    }
}
void outPutList(SortedList * result){
    for (int i = 0; i < result->size(); ++i) {
        cout << result->get(i) << " ";
    }
    cout << endl;
}
int main() {
    /*SortedList * list = new SortedList;
    list->add(76);
    list->add(45);
    list->add(78);
    list->add(0);
    list->add(77);
    list->add(76);

    list->printAll();

    list->remove(4);
    list->remove(0);
    list->remove(2);

    list->printAll();

    SortedUnicList * unicList = new SortedUnicList;
    unicList->add(34);
    unicList->add(0);
    unicList->add(34);
    unicList->add(99);
    unicList->add(34);
    unicList->add(0);
    unicList->add(23);

    unicList->printAll();*/
    //Task 1
    {
        SortedList *a = new SortedList;
        SortedList *b = new SortedList;
        fillList(a, b);
        SortedList *result = Union(a, b);
        outPutList(result);
    }
    //Task 2
    {
        SortedList *a = new SortedList;
        SortedList *b = new SortedList;
        fillList(a, b);
        SortedList * result = Intersect(a, b);
        outPutList(result);
    }
    //Task 3
    {
        SortedList *a = new SortedList;
        SortedList *b = new SortedList;
        fillList(a, b);
        SortedList * result = Difference(a, b);
        outPutList(result);
    }
}