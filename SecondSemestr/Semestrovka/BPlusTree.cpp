#include "BPlusTree.h"
#include "iostream"
#include "ctime"
#include "fstream"
using namespace std;

int t = 100;
struct Node{
    bool leaf = true;
    int size = 0;
    int * keys = new int[t];
    Node ** child = new Node *[t + 1];
};
struct BPLusTree{
    Node * root = nullptr;

    void putKey(Node * current, int key, string value){
        if(current->size == 0) {
            current->keys[0] = key;
        }
        else {
            for (int i = 0; i < current->size; ++i) {
                if(key < current->keys[i]){
                    for (int j = i; j < current->size; ++j) {
                        current->keys[j + 1] = current->keys[j];
                    }
                    current->keys[i] = key;
                }

            }
        }
    }
    Node * getLeaf(int key){
        Node * current = root;

        while(!current->leaf){
            for (int i = 0; i < current->size; ++i) {
                if(key < current->keys[i]){
                    current = current->child[i];
                    break;
                }
                if(i == current->size - 1){
                    current = current->child[i + 1];
                    break;
                }
            }
        }
        return current;
    }
    bool FindKey(int key){
        Node * current = getLeaf(key);

        for (int i = 0; i < current->size; ++i) {
            if(current->keys[i] == key){
                return true;
            }
        }
        return false;
    }
    Node * findLeaf(int key){
        Node * current = getLeaf(key);

        for (int i = 0; i < current->size; ++i) {
            if(current->keys[i] == key){
                return current;
            }
        }
    }

    void Insert(int key){
        if(root == nullptr){
            root = new Node;
            root->keys[0] = key;
            root->leaf = true;
            root->size = 1;
        }
        else {
            if (FindKey(key)) {
                return;
            }
            Node * current = root;
            Node * parent;
            while(!current->leaf){
                parent = current;
                for (int i = 0; i < current->size; ++i) {
                    if(key < current->keys[i]){
                        current = current->child[i];
                        break;
                    }
                    if(i == current->size - 1){
                        current = current->child[i + 1];
                        break;
                    }
                }
            }

            if(current->size < t) {
                int index = 0;
                while (key > current->keys[index] && index < current->size)
                    index++;
                for (int j = current->size; j > index; j--) {
                    current->keys[j] = current->keys[j - 1];
                }
                current->keys[index] = key;
                current->size++;
                current->child[current->size] = current->child[current->size - 1];
                current->child[current->size - 1] = nullptr;
            }
            else{
                Node *newLeaf = new Node;
                int virtualKey[t + 1];

                for (int i = 0; i < t; i++) {
                    virtualKey[i] = current->keys[i];
                }

                int i = 0, j;
                while (key > virtualKey[i] && i < t)
                    i++;
                for (int j = t + 1; j > i; j--) {
                    virtualKey[j] = virtualKey[j - 1];
                }

                virtualKey[i] = key;
                newLeaf->leaf = true;
                current->size = (t + 1) / 2;
                newLeaf->size = t + 1 - (t + 1) / 2;
                current->child[current->size] = newLeaf;
                newLeaf->child[newLeaf->size] = current->child[t];
                current->child[t] = nullptr;
                for (i = 0; i < current->size; i++) {
                    current->keys[i] = virtualKey[i];
                }
                for (i = 0, j = current->size; i < newLeaf->size; i++, j++) {
                    newLeaf->keys[i] = virtualKey[j];
                }
                if (current == root) {
                    Node *newRoot = new Node;
                    newRoot->keys[0] = newLeaf->keys[0];
                    newRoot->child[0] = current;
                    newRoot->child[1] = newLeaf;
                    newRoot->leaf = false;
                    newRoot->size = 1;
                    root = newRoot;
                }
                else{
                    insertInternalNode(newLeaf->keys[0], parent, newLeaf);
                }
            }
        }
    }
    void insertInternalNode(int key, Node * current, Node * child){
        if (current->size < t) {
            int index = 0;
            while (key > current->keys[index] && index < current->size)
                index++;
            for (int j = current->size; j > index; j--) {
                current->keys[j] = current->keys[j - 1];
            }
            for (int j = current->size + 1; j > index + 1; j--) {
                current->child[j] = current->child[j - 1];
            }
            current->keys[index] = key;
            current->size++;
            current->child[index + 1] = child;
        } else {
            Node *newInternal = new Node;
            int virtualKey[t + 1];
            Node *virtualPtr[t + 2];
            for (int i = 0; i < t; i++) {
                virtualKey[i] = current->keys[i];
            }
            for (int i = 0; i < t + 1; i++) {
                virtualPtr[i] = current->child[i];
            }
            int i = 0, j;
            while (key > virtualKey[i] && i < t)
                i++;
            for (int j = t + 1; j > i; j--) {
                virtualKey[j] = virtualKey[j - 1];
            }
            virtualKey[i] = key;
            for (int j = t + 2; j > i + 1; j--) {
                virtualPtr[j] = virtualPtr[j - 1];
            }
            virtualPtr[i + 1] = child;
            newInternal->leaf = false;
            current->size = (t + 1) / 2;
            newInternal->size = t - (t + 1) / 2;
            for (i = 0, j = current->size + 1; i < newInternal->size; i++, j++) {
                newInternal->keys[i] = virtualKey[j];
            }
            for (i = 0, j = current->size + 1; i < newInternal->size + 1; i++, j++) {
                newInternal->child[i] = virtualPtr[j];
            }
            if (current == root) {
                Node *newRoot = new Node;
                newRoot->keys[0] = current->keys[current->size];
                newRoot->child[0] = current;
                newRoot->child[1] = newInternal;
                newRoot->leaf = false;
                newRoot->size = 1;
                root = newRoot;
            } else {
                insertInternalNode(current->keys[current->size], getParent(root, current), newInternal);
            }
        }
    }
    Node * getParent(Node * current, Node * child){
        Node *parent;
        if (current->leaf || (current->child[0])->leaf) {
            return nullptr;
        }
        for (int i = 0; i < current->size + 1; i++) {
            if (current->child[i] == child) {
                parent = current;
                return parent;
            } else {
                parent = getParent(current->child[i], child);
                if (parent != nullptr)
                    return parent;
            }
        }
        return parent;
    }
    void DeleteKey(int key){
        Node * current = root;
        Node * parent;
        int i = 0;
        while (current->leaf == false) {
            parent = current;
            for (int j = 0; j < current->size; j++) {
                if (key < current->keys[j]) {
                    current = current->child[j];
                    i = j;
                    break;
                }
                if (j == current->size - 1) {
                    current = current->child[j + 1];
                    break;
                }
            }
        }
        // Если это единственный элемент
        if (current->size == 1) {
            // Если самый левый
            if (i == 0) {
                current->keys[0] = parent->child[i + 1]->keys[0];
                current->child[1] = parent->child[i + 1]->child[parent->child[i + 1]->size];
                if (parent->child[i+1]->size > 1) {
                    current->child[1] = parent->child[i + 1]->child[0];
                    current = parent->child[i + 1];
                    for (int k = 0; k < current->size; k++) {
                        current->keys[k] = current->keys[k + 1];
                    }
                    current->child[current->size - 1] = current->child[current->size];
                    current->child[current->size] = nullptr;
                    current->size--;
                    for (int k = 0; k < parent->size; k++) {
                        parent->keys[k] = parent->child[k+1]->keys[0];
                    }
                }
            }
            // Если любой другой
            if (i > 0) {
                if (parent->child[i-1]->size > 1) {
                    current->keys[0] = parent->child[i - 1]->keys[parent->child[i - 1]->size];
                    for (int k = 0; k < parent->size; k++) {
                        parent->keys[k] = parent->child[k+1]->keys[0];
                    }
                    current = parent->child[i - 1];
                }
                if (parent->child[i+1]->size > 1) {
                    current->keys[0] = parent->child[i + 1]->keys[0];
                    current = parent->child[i + 1];
                    for (int k = 0; k < current->size; k++) {
                        current->keys[k] = current->keys[k + 1];
                    }
                    for (int k = 0; k < parent->size; k++) {
                        parent->keys[k] = parent->child[k+1]->keys[0];
                    }
                }
                current->child[current->size - 1] = current->child[current->size];
                current->child[current->size] = nullptr;
                current->size--;
            }
        }
        if (current->size > 1) {
            int j = 0;
            while (j < t && key != current->keys[j]) j++;
            for (int k = j; k < t-1; k++) {
                current->keys[k] = current->keys[k + 1];
            }
            for (int k = 0; k < parent->size; k++) {
                parent->keys[k] = parent->child[k+1]->keys[0];
            }
            current->child[current->size - 1] = current->child[current->size];
            current->child[current->size] = nullptr;
            current->size--;
        }
        // не реализовал удаление из родительских узлов
    }
};
int main(){
    /*ofstream fout;
    fout.open("data.txt");
    for (int i = 1; i <= 50; ++i) {
        for (int j = 1; j <= 100 * i; ++j) {
            fout << rand() << " ";
        }
        fout << endl;
    }
    fout.close();*/

    ifstream fin;
    fin.open("data.txt");
    BPLusTree * tree = new BPLusTree;

    double array[50][5000];
    for (int i = 0; i < 50; ++i) {
        for (int j = 0; j < 100 * (i + 1); ++j) {
            int key;
            fin >> key;
            array[i][j] = key;
        }
    }
    fin.close();

    for (int i = 0; i < 50; ++i) {
        double startTime = clock();

        for (int j = 0; j < 100 * (i + 1); ++j) {
            tree->Insert(array[i][j]);
        }
        double endTime = clock();
        double totalTime = endTime - startTime;
        cout << totalTime << " ";
    }
    cout << endl;

    for (int i = 0; i < 50; ++i) {
        double startTime = clock();

        for (int j = 0; j < 100 * (i + 1); ++j) {
            tree->FindKey(array[i][j]);
        }
        double endTime = clock();
        double totalTime = endTime - startTime;
        cout << totalTime << " ";
    }
    cout << endl;

    for (int i = 0; i < 50; ++i) {
        double startTime = clock();

        for (int j = 0; j < 100 * (i + 1); ++j) {
            tree->FindKey(array[i][j]);
        }
        double endTime = clock();
        double totalTime = endTime - startTime;
        cout << totalTime << endl;
    }
    return 0;
}