#ifndef DARK_BINSEARCH_H
#define DARK_BINSEARCH_H

int findMaxElement(int **array, int n, int m);
int findMinElement(int **array, int n, int m);
bool taskTwoFunction(int n, int m, int x, int **array);
int taskThreeFunction(int x);
int taskFourFunction(int n, int m, int **array);
int * findLessMoreQuantity(int **array, int x, int n, int m);
#endif //DARK_BINSEARCH_H
