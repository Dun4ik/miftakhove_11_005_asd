#include "sixthDz.h"
#include "iostream"
using namespace std;
struct MaxHeap{
    int * array;
    int size = 0;
    int maxSize = 0;

    int Parent(int i){ return (i - 1) / 2; }
    int LeftChild(int i){ return 2 * i + 1; }
    int RightChild(int i){ return 2 * i + 2; }

    void swap (int i, int j){
        int t = array[i];
        array[i] = array[j];
        array[j] = t;
    }
    void SiftUp(int i){
        while(i > 0 && array[i] > array[Parent(i)]){
            swap(i, Parent(i));
            i = Parent(i);
        }
    }
    void SiftDown(int i){
        int maxIndex = i;
        if(array[LeftChild(i)] > array[maxIndex] && LeftChild(i) < size)
            maxIndex = LeftChild(i);
        if(array[RightChild(i)] > array[maxIndex] && RightChild(i) < size)
            maxIndex = RightChild(i);
        if(maxIndex != i){
            swap(i, maxIndex);
            SiftDown(maxIndex);
        }
    }
    void expandMax(){
        int lastSize = maxSize;
        int * currentArray = array;
        if(maxSize == 0)
            maxSize = 2;
        else
            maxSize *= 2;
        int * newArray = new int[maxSize];
        for (int i = 0; i < maxSize; ++i)
            if(i < lastSize)
                newArray[i] = array[i];
            else
                newArray[i] = 0;
        array = newArray;
        delete currentArray;
    }
    void Insert(int p){
        if(size < maxSize) {
            size++;
            array[size - 1] = p;
            SiftUp(size - 1);
        }
        else {
            expandMax();
            Insert(p);
        }
    }
    int ExtractMax(){
        int result = array[0];
        swap(0, size - 1);
        size--;
        SiftDown(0);
        return result;
    }
    void printAll(){
        for (int i = 0; i < size; ++i) {
            cout << array[i] << " ";
        }
        cout << endl;
    }
};
int main(){
    int * sortedArray = new int[8];
    MaxHeap * heap = new MaxHeap;
    for (int i = 0; i < 8; ++i) {
        int x;
        cin >> x;
        heap->Insert(x);
    }
    for (int i = 7; i >= 0; --i)
        sortedArray[i] = heap->ExtractMax();
    for (int i = 0; i < 8; ++i)
        cout << sortedArray[i] << " ";
    cout << endl;

    delete[] sortedArray;
    delete heap;
    return 0;
}